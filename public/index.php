<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
/*require '../Core/Router.php';

require '../App/Controllers/Posts.php';*/

/*Twig*/

require_once dirname(__DIR__) . '/vendor/autoload.php';
/*Twig_AutoLoader::register();*/

/*spl_autoload_register(function ($class) {

	$root = dirname(__DIR__);

	$file = $root . '/' . str_replace('\\', '/', $class) . '.php';

	if (is_readable($file)) {

		require $root . '/' . str_replace('\\', '/', $class) . '.php';

	}
});*/

error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

$router = new Core\Router();

$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('login', ['controller' => 'Auth\Logins', 'action' => 'index']);
$router->add('logged', ['controller' => 'Auth\Logins', 'action' => 'checkLogged']);
//$router->add('register', ['controller' => 'Auth\Registers', 'action' => 'index']);
//$router->add('registered', ['controller' => 'Auth\Registers', 'action' => 'saveUser']);
$router->add('logout', ['controller' => 'Auth\Logins', 'action' => 'logout']);
$router->add('profile', ['controller' => 'Auth\Logins', 'action' => 'getProfile']);
$router->add('howto', ['controller' => 'Home', 'action' => 'howToUse']);

//$router->add('posts', ['controller' => 'Posts', 'action' => 'index']);



//$router->add('savedata', ['controller' => 'Users', 'action' => 'savedata']);

/*$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('posts', ['controller' => 'Posts', 'action' => 'index']);
$router->add('posts/new', ['controller' => 'Posts', 'action' => 'new']);
$router->add('login', ['controller' => 'Auth\Logins', 'action' => 'index']);*/
/*$router->add('{controller}/{action}');*/
//$router->add('admin/{action}/{controller}');
/*$router->add('{controller}/{id:\d+}/{action}');
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);*/

/*echo '<pre>';
echo var_dump($router->getRoutes());
echo '</pre>';
*/

/*$url = $_SERVER['QUERY_STRING'];

if ($router->match($url)) {
	
	echo '<pre>';
	echo var_dump($router->getParams());
	echo '</pre>';

} else {

	echo 'No route found';
}*/

$router->dispatch($_SERVER['QUERY_STRING']);

?>