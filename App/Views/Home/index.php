<!DOCTYPE html>
<html>
<head>
	<title>Medi App</title>
</head>
<body>

<h1>Welcome <?php echo htmlspecialchars($name); ?> to MediApp</h1>

<ul>
	<?php foreach ($colors as $color): ?>
		<li><?php echo htmlspecialchars($color); ?></li>
	<?php endforeach; ?>
</ul>

</body>
</html>