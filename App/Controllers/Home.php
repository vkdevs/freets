<?php

namespace App\Controllers;

use \Core\View;

class Home extends \Core\Controller
{
	
	public function indexAction()
	{

		//echo 'This is Home index';
		/*View::render('Home/index.php', [
			'name' => 'Dilan',
			'colors' => ['red', 'blue', 'black']*/

		/*Twig*/
		View::renderTemplate('Home/index.html', [
			'name' => 'Dilan',
			'colors' => ['red', 'blue', 'black'],
			'session'   => $_SESSION
		]);

	}

	public function howToUse()
	{

		View::renderTemplate('Home/howtouse.html');

	}

	protected function before()
	{

		//echo '(before)';
		//return false;

	}

	protected function after()
	{

		//echo '(after)';

	}
}

?>