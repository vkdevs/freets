<?php

namespace App\Controllers\Auth;

use \Core\View;
use App\Models\Auth\Register;

class Registers extends \Core\Controller
{
	
	public function indexAction()
	{

		View::renderTemplate('Auth\register.html', [
			'path' => 'registered',
			'action' => 'saveUser'
		]);

	}

	public function saveUser()
	{

		if (!empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password'])) {
			
			$user = $_POST['username'];

			$pass = $_POST['password'];

			$email = $_POST['email'];

			$registered = Register::trySave($user, $pass, $email);

			if ($registered == true) {
				
				$_SESSION['success_message'] = 'User Create Successfully.';

				/*View::renderTemplate('Auth\login.html', [
					'msg' => $_SESSION['success_message']
				]);*/
				header('Location: login');

				//$_SESSION['success_message'] = NULL;

			} else {

				$_SESSION['error_message'] = 'User Create Failed.';

				View::renderTemplate('Auth\register.html', [
					'msg' => $_SESSION['error_message']
				]);

				$_SESSION['error_message'] = NULL;

			}

		} else {

			header('Location: register');
		}
	}

	public function editAction()
	{

		echo '<pre>'. htmlspecialchars(print_r($this->route_params, true)). '</pre>';

	}
}

?>