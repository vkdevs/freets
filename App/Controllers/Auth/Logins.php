<?php

namespace App\Controllers\Auth;

use \Core\View;
use App\Models\Auth\Login;

class Logins extends \Core\Controller
{
	
	public function indexAction()
	{

		View::renderTemplate('Auth\login.html', [
			'path' => 'logged',
			'action' => 'checkLogged',
			'session' => $_SESSION
		]);

	}

	public function checkLogged()
	{

		$email = $_POST['email'];

		$pass = $_POST['password'];

		$logged = Login::tryLogin($email, $pass);

		if ($logged['status'] == true) {
			
			$_SESSION['user_id'] = $logged['user_id'];

			$user_id = $_SESSION['user_id'];

			header('Location: profile');

		} else {

			echo 'Nooo';
		}
	}

	public function editAction()
	{

		echo '<pre>'. htmlspecialchars(print_r($this->route_params, true)). '</pre>';

	}

	public function logout()
	{

		session_unset();
		session_destroy();

		header('Location: login');

	}

	public function getProfile()
	{

		$user_id = $_SESSION['user_id'];

		$user = Login::getUserData($user_id);

		View::renderTemplate('Users\index.html', [
			'user' => $user,
			'session' => $_SESSION
		]);
	}
}

?>