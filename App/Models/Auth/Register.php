<?php

namespace App\Models\Auth;

use PDO;

class Register extends \Core\Model
{

	public static function trySave($username, $password, $email)
	{

		$user_type = 1;

		try {

			$db = static::getDB();

			$sql = 'SELECT * FROM user_logins JOIN user_details ON user_logins.user_id = user_details.user_detail_id WHERE user_logins.user_name = :user_name OR user_details.email = :email';

			$stmt = $db->prepare($sql);

			$stmt->bindParam(':user_name', $username);

			$stmt->bindParam(':email', $email);

			$stmt->execute();

			if ($stmt->rowCount() > 0) {

				return false;

			} else {

				$sql = 'INSERT INTO user_logins (user_name, user_pass, user_type) VALUES (:user_name, :user_pass, :user_type)';

				$stmt = $db->prepare($sql);

				$stmt->bindParam(':user_name', $username);

				$stmt->bindParam(':user_pass', $password);

				$stmt->bindParam(':user_type', $user_type);

				if ($stmt->execute()) {

					$reg_user_id = $db->lastInsertId();
					
					$sql = 'INSERT INTO user_details (user_detail_id, email) VALUES (:user_detail_id, :email)';

					$stmt = $db->prepare($sql);

					$stmt->bindParam(':email', $email);

					$stmt->bindParam(':user_detail_id', $reg_user_id);

					if ($stmt->execute()) {
						
						return true;
					}
				}
			}

		} catch (PDOException $e) {

			echo $e->getMessage();
		}
	}
}

?>