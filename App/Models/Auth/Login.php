<?php

namespace App\Models\Auth;

use PDO;

class Login extends \Core\Model
{

	public static function tryLogin($email, $password)
	{

		try {

			$db = static::getDB();

			$sql = 'SELECT * FROM user_logins JOIN user_details ON user_logins.user_id = user_details.user_detail_id WHERE user_details.email = :email AND user_logins.user_pass = :user_pass';

			$stmt = $db->prepare($sql);

			$stmt->bindParam(':email', $email);

			$stmt->bindParam(':user_pass', $password);

			$stmt->execute();

			if ($stmt->rowCount() > 0) {

				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				
				$userLogged = ['status' => true, 'user_id' => $result['user_id']];

				return $userLogged;

			} else {

				return false;
			}

		} catch (PDOException $e) {

			echo $e->getMessage();
		}
	}

	public static function getUserData($user_id)
	{
		
		try {

			$db = static::getDB();

			$sql = 'SELECT * FROM user_logins JOIN user_details ON user_logins.user_id = user_details.detail_id WHERE user_id = :user_id';

			$stmt = $db->prepare($sql);

			$stmt->bindParam(':user_id', $user_id);

			$stmt->execute();

			if ($stmt->rowCount() > 0) {

				$result = $stmt->fetch(PDO::FETCH_ASSOC);

				return $result;

			} else {

				return false;
			}

		} catch (PDOException $e) {

			echo $e->getMessage();
		}

	}
}

?>